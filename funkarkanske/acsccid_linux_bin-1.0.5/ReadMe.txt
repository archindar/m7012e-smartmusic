ACS CCID PC/SC Driver for Linux
Binary Packages for Linux Distributions
Advanced Card Systems Ltd.



Contents
----------------

   1. Release Notes
   2. Installation
   3. History
   4. File Contents
   5. Limitations
   6. Support



1. Release Notes
----------------

Version: 1.0.5
Release Date: 11/9/2013

Supported Readers
-----------------

CCID Readers

VID  PID  Reader              Reader Name
---- ---- ------------------- -----------------------------
072F 8300 ACR33U-A1           ACS ACR33U-A1 3SAM ICC Reader
072F 8302 ACR33U-A2           ACS ACR33U-A2 3SAM ICC Reader
072F 8307 ACR33U-A3           ACS ACR33U-A3 3SAM ICC Reader
072F 8301 ACR33U              ACS ACR33U 4SAM ICC Reader
072F 90CC ACR38U-CCID         ACS ACR38U-CCID
072F 90CC ACR100-CCID         ACS ACR38U-CCID
072F 90D8 ACR3801             ACS ACR3801
072F B100 ACR39U              ACS ACR39U ICC Reader
072F B101 ACR39K              ACS ACR39K ICC Reader
072F B102 ACR39T              ACS ACR39T ICC Reader
072F B103 ACR39F              ACS ACR39F ICC Reader
072F B104 ACR39U-SAM          ACS ACR39U-SAM ICC Reader
072F B000 ACR3901U            ACS ACR3901U ICC Reader
072F 90D2 ACR83U-A1           ACS ACR83U
072F 8306 ACR85               ACS ACR85 PINPad Reader
072F 2011 ACR88U              ACS ACR88U
072F 8900 ACR89U-A1           ACS ACR89 ICC Reader
072F 8901 ACR89U-A2           ACS ACR89 Dual Reader
072F 8902 ACR89U-A3           ACS ACR89 FP Reader
072F 1205 ACR100I             ACS ACR100 ICC Reader
072F 1204 ACR101              ACS ACR101 ICC Reader
072F 1206 ACR102              ACS ACR102 ICC Reader
072F 2200 ACR122U             ACS ACR122U
072F 2200 ACR122U-SAM         ACS ACR122U
072F 2200 ACR122T             ACS ACR122U
072F 2214 ACR1222U-C1         ACS ACR1222 1SAM PICC Reader
072F 1280 ACR1222U-C3         ACS ACR1222 1SAM Dual Reader
072F 2207 ACR1222U-C6         ACS ACR1222 Dual Reader
072F 222B ACR1222U-C8         ACS ACR1222 1SAM PICC Reader
072F 2206 ACR1222L-D1         ACS ACR1222 3S PICC Reader
072F 222E ACR123U             ACS ACR123 3S Reader
072F 2237 ACR123U             ACS ACR123 PICC Reader
072F 2219 ACR123U Bootloader  ACS ACR123US_BL
072F 2203 ACR125              ACS ACR125 nPA plus
072F 221A ACR1251U-A1         ACS ACR1251 1S CL Reader
072F 2229 ACR1251U-A2         ACS ACR1251 CL Reader
072F 222D [OEM Reader]        [OEM Reader Name]
072F 2218 ACR1251U-C (SAM)    ACS ACR1251U-C Smart Card Reader
072F 221B ACR1251U-C          ACS ACR1251U-C Smart Card Reader
072F 2100 ACR128U             ACS ACR128U
072F 2224 ACR1281U-C1         ACS ACR1281 1S Dual Reader
072F 220F ACR1281U-C2 (qPBOC) ACS ACR1281 CL Reader
072F 2223 ACR1281U    (qPBOC) ACS ACR1281 PICC Reader
072F 2208 ACR1281U-C3 (qPBOC) ACS ACR1281 Dual Reader
072F 0901 ACR1281U-C4 (BSI)   ACS ACR1281 PICC Reader
072F 220A ACR1281U-C5 (BSI)   ACS ACR1281 Dual Reader
072F 2215 ACR1281U-C6         ACS ACR1281 2S CL Reader
072F 2220 ACR1281U-C7         ACS ACR1281 1S PICC Reader
072F 2233 ACR1281U-K          ACS ACR1281U-K PICC Reader
072F 2234 ACR1281U-K          ACS ACR1281U-K Dual Reader
072F 2235 ACR1281U-K          ACS ACR1281U-K 1S Dual Reader
072F 2236 ACR1281U-K          ACS ACR1281U-K 4S Dual Reader
072F 2213 ACR1283L-D1         ACS ACR1283 4S CL Reader
072F 222C ACR1283L-D2         ACS ACR1283 CL Reader
072F 220C ACR1283 Bootloader  ACS ACR1283U FW Upgrade
072F 0102 AET62               ACS AET62 PICC Reader
072F 0103 AET62               ACS AET62 1SAM PICC Reader
072F 0100 AET65               ACS AET65 ICC Reader
072F 8201 APG8201-A1          ACS APG8201
072F 8202 [OEM Reader]        [OEM Reader Name]
072F 90DB CryptoMate64        ACS CryptoMate64

non-CCID Readers

VID  PID  Reader              Reader Name
---- ---- ------------------- -----------------------------
072F 9000 ACR38U              ACS ACR38U
072F 90CF ACR38U-SAM          ACS ACR38U-SAM
072F 90CE [OEM Reader]        [OEM Reader Name]
072F 0101 AET65               ACS AET65 1SAM ICC Reader
072F 9006 CryptoMate          ACS CryptoMate

Supported Linux Distributions
-----------------------------

Debian
- squeeze (6.0)
- wheezy  (7.0)

Ubuntu
- lucid   (10.04LTS)
- precise (12.04LTS)
- quantal (12.10)
- raring  (13.04)

Fedora 18/19

openSUSE 12.2/12.3



2. Installation
---------------

1. Before installing the driver, unplug the reader first and make sure that you
   have installed pcsc-lite package for your Linux distribution.

2. Login as root. If you are already login as normal user, enter "su" command to
   become administrator. In Ubuntu, you can use "sudo" command with package
   installation.

3. To install the driver, enter the following commands:

Debian/Ubuntu
# dpkg -i [package filename]

Fedora/openSUSE
# rpm -ivh [package filename]

4. Restart pcscd service. If the pcscd service cannot be restarted, reboot the
   system.

# /etc/init.d/pcscd restart

5. Plug the reader to the system. You can list the readers from your PC/SC
   application.

6. To uninstall the driver, enter the following commands:

Debian/Ubuntu
# dpkg -r [package name]

Fedora/openSUSE
# rpm -e [package name]



3. History
----------

v1.0.0 (9/9/2009)
1. New release.
2. Based on ccid-1.3.11 (http://pcsclite.alioth.debian.org/ccid.html).
3. Add source and binary packages for Fedora 11, openSUSE 11.1 and Debian 5.0.

v1.0.1 (9/11/2009)
1. Update driver to v1.0.1.

v1.0.2 (17/6/2010)
1. Update driver to v1.0.2.
2. Update source and binary packages for Fedora 13, openSUSE 11.2 and
   Debian 5.0.

v1.0.2 (24/11/2010)
1. Update driver to v1.0.2.
2. Add binary packages for Debian 5.0, Ubuntu 9.04/10.04 and openSUSE 11.1/11.2.

v1.0.2 (29/11/2010)
1. Add binary packages for Ubuntu 9.10/10.10 and openSUSE 11.3.

v1.0.2 (16/3/2011)
1. Add binary packages for Debian 5.0, Ubuntu 10.10, Fedora 14 and
   openSUSE 11.3.

v1.0.3 (6/2/2012)
1. Add binary packages for Debian 5.0/6.0 and Ubuntu 8.04 - 11.10.

v1.0.3 (7/2/2012)
1. Add binary packages for openSUSE 11.1 - 12.1.

v1.0.3 (17/2/2012)
1. Add binary packages for Fedora 14 - 16.

v1.0.4 (10/7/2012)
1. Add binary packages for Debian 6.0/wheezy, Ubuntu 8.04LTS/10.04LTS/11.04/
   11.10/12.04LTS/quantal, Fedora 14/15/16/17 and openSUSE 11.4/12.1.

v1.0.5 (11/9/2013)
1. Add binary packages for Debian 6.0/7.0, Ubuntu 10.04LTS/12.04LTS/12.10/13.04,
   Fedora 18/19 and openSUSE 12.2/12.3.



4. File Contents
----------------

|   ReadMe.txt
|
+---debian
|   +---squeeze
|   |       libacsccid1_1.0.5-1~bpo60+1_amd64.deb
|   |       libacsccid1_1.0.5-1~bpo60+1_i386.deb
|   |
|   \---wheezy
|           libacsccid1_1.0.5-1~bpo70+1_amd64.deb
|           libacsccid1_1.0.5-1~bpo70+1_i386.deb
|
+---fedora
|       pcsc-lite-acsccid-1.0.5-1.fc18.i686.rpm
|       pcsc-lite-acsccid-1.0.5-1.fc18.x86_64.rpm
|       pcsc-lite-acsccid-1.0.5-1.fc19.i686.rpm
|       pcsc-lite-acsccid-1.0.5-1.fc19.x86_64.rpm
|
+---opensuse
|   +---12.2
|   |       pcsc-acsccid-1.0.5-22.1.i586.rpm
|   |       pcsc-acsccid-1.0.5-22.1.x86_64.rpm
|   |
|   \---12.3
|           pcsc-acsccid-1.0.5-22.1.i586.rpm
|           pcsc-acsccid-1.0.5-22.1.x86_64.rpm
|
\---ubuntu
    +---lucid
    |       libacsccid1_1.0.5-1~lucid1_amd64.deb
    |       libacsccid1_1.0.5-1~lucid1_i386.deb
    |
    +---precise
    |       libacsccid1_1.0.5-1~precise1_amd64.deb
    |       libacsccid1_1.0.5-1~precise1_i386.deb
    |
    +---quantal
    |       libacsccid1_1.0.5-1~quantal1_amd64.deb
    |       libacsccid1_1.0.5-1~quantal1_i386.deb
    |
    \---raring
            libacsccid1_1.0.5-1~raring1_amd64.deb
            libacsccid1_1.0.5-1~raring1_i386.deb



5. Limitations
--------------



6. Support
----------

In case of problem, please contact ACS through:

Web Site: http://www.acs.com.hk/
E-mail: info@acs.com.hk
Tel: +852 2796 7873
Fax: +852 2796 1286



-----------------------------------------------------------------


Copyright
Copyright by Advanced Card Systems Ltd. (ACS) No part of this reference manual
may be reproduced or transmitted in any from without the expressed, written
permission of ACS.

Notice
Due to rapid change in technology, some of specifications mentioned in this
publication are subject to change without notice. Information furnished is
believed to be accurate and reliable. ACS assumes no responsibility for any
errors or omissions, which may appear in this document.
