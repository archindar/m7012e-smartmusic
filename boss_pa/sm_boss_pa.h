#ifndef SM_BOSS_PA_H
#define	SM_BOSS_PA_H

int sm_boss_pa_load(); // loads list of current pulseaudio sinks

int sm_boss_pa_getsinkindex(const char* ip); // return sink index for ip (< 0 if error occured)

#endif	/* SM_BOSS_PA_H */

