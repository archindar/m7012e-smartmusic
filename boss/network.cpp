#include "network.h"
#include "sm_boss_pa.h"
#include "pulsecommander.h"

#include <QDebug>
#include <QDataStream>
#include <QNetworkInterface>
#include <QBuffer>

extern "C" {
    int sm_boss_pa_load(); // loads list of current pulseaudio sinks

    int sm_boss_pa_getsinkindex(const char* ip);
}

network::network(QObject *parent, quint16 port) :
    QObject(parent)
{
    tcpserver = new QTcpServer(); //Create the tcpserver
    tcpserver->listen(QHostAddress::Any, port);
    connect(tcpserver, SIGNAL(newConnection()), this, SLOT(handleIncomming()));

    localSink = "alsa_output.pci-0000_00_1b.0.analog-stereo";

    //QList<QNetworkAddressEntry> entries = iface.addressEntries();
    //qDebug()<< entries.first().broadcast();
    //foreach (QNetworkAddressEntry n, entries) {
    //    qDebug()<<n.broadcast();
    //}

    //QNetworkInterface iface = QNetworkInterface::interfaceFromName("wlan0");
    //QList<QNetworkAddressEntry> entries = iface.addressEntries();
    //regListener = new QUdpSocket(); //Create the udp socket that listens for registration messages
    //regListener->bind(entries.first().broadcast(), 20202);

    qDebug()<<"Com initiated";


}

network::~network(){
    delete ch1;
}

void network::startConnection(QHostAddress recadr, quint16 recport, quint16 ourport)
{

    QTcpSocket *socket = new QTcpSocket();
    socket->bind(ourport);
    qDebug()<< socket->localPort();
    socket->connectToHost(recadr, recport);
    connect(socket, SIGNAL(connected()), this, SLOT(conEstablished()));
    listOfSockets.push_front(socket);
}

void network::handleIncomming() //If not initiator of communication
{
    qDebug() << "New comm detected";
    QObject::sender()->dumpObjectInfo();

    QTcpSocket *socket = tcpserver->nextPendingConnection();
    socket->
    connect(socket, SIGNAL(readyRead()), this, SLOT(readPortMsg())); //Connect so we can read messages
    connect(socket, SIGNAL(disconnected()), this, SLOT(remoteDisconnected())); //connect the disconnect procedure
    listOfSockets.push_front(socket);
}

void network::readPortMsg()
{
    QByteArray db;
    QString msg;

    QTcpSocket *sender = (QTcpSocket*)QObject::sender();
    qDebug()<<sender->peerAddress();

    //msg = "{\"encoding\" : \"UTF-8\",\"plug-ins\" : [\"python\",\"c++\",\"ruby\"],\"indent\" : { \"length\" : 3, \"use_space\" : true }}";
    msg = sender->readAll(); //Read the message from the socket



    QJsonDocument jsonResponse = QJsonDocument::fromJson(msg.toUtf8());
    //QJsonObject jsonObject = jsonResponse.object();
    QVariantMap result = jsonResponse.toVariant().toMap();

    //########### EMPLOYEE tag read, change the stream;
    if(result["command"].toString()=="tagRead"){
        qDebug() << "ip of reader:" << QHostAddress(result["clientip"].toString());
        QString ip = sender->peerAddress().toString();
        int sinkindex;
        pulsecommander *pcm = new pulsecommander();
        //##### Local tag
        if (ip == sender->localAddress().toString()){ //compare the ip of the sender to ours
            qDebug()<<"Det var lokalt"<<ip;
            pcm->changeSink(localSink);
        }
        //##### Remote tag
        else {
            if(sm_boss_pa_load()){
                qDebug()<<"FEL could not load";
            }
            sinkindex = sm_boss_pa_getsinkindex(result["clientip"].toString().toStdString().c_str());
            if(sinkindex<0){
                qDebug()<<"No sinks found";
                return; //stop from going längre
            }
            pcm->changeSink(sinkindex);
            qDebug()<< "Sink id: " << sinkindex;
        }


        qDebug() << "tagID:" << result["userid"].toString();
    }

    //########### WEB get list of sink names
    else if(result["command"].toString()=="getSinks"){
        qDebug() << "Sink request";
        QString ans = "{ \"sinks\" : [ \"alsa_output.pci-0000_00_1b.0.analog-stereo\",  \"tunnel.raspberrypi.local.alsa_output.platform-bcm2835_AUD0.0.analog-stereo\"]}";
        QJsonDocument jsonAns = QJsonDocument::fromJson(ans.toUtf8());
        if(sender->isWritable()){
            sender->write(ans.toUtf8());
            qDebug()<< "request answered";
        }
    }

    //########### WEB change sink
    else if (result["command"].toString()=="changeSink"){
        qDebug()<<"Change of sink request";
        qDebug()<<result["sinkName"].toString();
        pulsecommander *pcm = new pulsecommander();
        pcm->changeSink(result["sinkName"].toString()); //move the inputs to the sink in the messages
    }


}


void network::conEstablished() //If initiator of communication
{
    qDebug() << "Connection Established";
    connect(ch1->listener, SIGNAL(readyRead()), this, SLOT(readPortMsg())); //Connect so we can read messages
    ch1->ip = ch1->listener->peerAddress().toString();
    this->sendPortMsg();
}

void network::remoteDisconnected() //Connected when the remote initiade the connection
{ //Handels the procedure that should be done in that case.
    QTcpSocket *sender = (QTcpSocket*)QObject::sender(); //retrievs the socket that has disconnected

    qDebug()<<"Connection closed";
    sender->close();
    sender->deleteLater();
    if(listOfSockets.removeOne(sender)){ //Removes the pointer in the list of sockets
        qDebug()<<"socket removed";
    }
}

void network::sendPortMsg()
{

}
