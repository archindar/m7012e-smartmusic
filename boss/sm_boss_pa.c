#include "sm_boss_pa.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef int (*lineprocfunc_t)(char* linebuffer);

int readLines(lineprocfunc_t linecallback)
{
    FILE * pFile;
    pFile = fopen (".tempsinks","r");
    if (pFile == NULL)
    {
        printf("file errors happened\n");
        return -2;
    }
    
    int c;
    int size = 1024;
    char *buffer = (char *)malloc(size);
    do { // read all lines in file
      int pos = 0;
      do{ // read one line
        c = fgetc(pFile);
        if(c != EOF) buffer[pos++] = (char)c;
        if(pos >= size - 1) { // increase buffer length - leave room for 0
          size *=2;
          buffer = (char*)realloc(buffer, size);
        }
      }while(c != EOF && c != '\n');
      buffer[pos] = 0;
      
      // iterate lines here
      if (linecallback(buffer))
        return -1;
      
    } while(c != EOF); 
    
    fclose(pFile);
    return 0;
}


int sinkindex;
int lastsinkindex;
char* sinkip = 0;
int getsinkindex_callback(char* linebuffer)
{
    const char* sinkindex_proto = "Sink #";
    if (strncmp(linebuffer, sinkindex_proto, strlen(sinkindex_proto)) == 0)
    {
        lastsinkindex = atoi(linebuffer + strlen(sinkindex_proto));
        
        return 0;
    }
    
    const char* sinkprop_tunnelremoteserver_proto = "\t\ttunnel.remote.server = \"[";
    if (strncmp(linebuffer, sinkprop_tunnelremoteserver_proto, strlen(sinkprop_tunnelremoteserver_proto)) == 0)
    {
        const char* ip = linebuffer + strlen(sinkprop_tunnelremoteserver_proto);
        
        char* ip_mod = (char*)ip;
        while (*ip_mod)
        {
            if (*ip_mod == ']')
            {
                *ip_mod = 0;
                break;
            }
            
            ip_mod++;
        }
        
        if (strcmp(sinkip, ip) == 0)
            sinkindex = lastsinkindex;
        
        return 0;
    }
    
    return 0;
}


int sm_boss_pa_load()
{
    int errno;
    
    errno = system("pactl list sinks > .tempsinks");
    if (errno)
    {
        fprintf(stderr, "error creating pactl output, %i\n", errno);
        return -1;
    }
    
    return 0;
}

int sm_boss_pa_getsinkindex(const char* ip)
{
    sinkindex = -1;
    lastsinkindex = -1;
    sinkip = (char*)ip;

    
    if (readLines(getsinkindex_callback))
        return -1;
    
    if (sinkindex < 0)
    {
        fprintf(stderr, "error searching for sinkindex, %i\n", sinkindex);
        return -2;
    }
    
    return sinkindex;
}
