#include "pulsecommander.h"
#include <QRegularExpression>

pulsecommander::pulsecommander(QObject *parent) :
    QObject(parent)
{
}

void pulsecommander::changeSink(QString sinkName)
{
    QString program = "/bin/sh";
    QStringList arguments;

    arguments << "-c" << "pactl list sink-inputs | grep 'Sink Input' | cut -d\"#\" -f2";
    QProcess *getInputsProcess = new QProcess(); //Process that runs the command.
    getInputsProcess->setProperty("sinkid", QVariant(sinkName)); //Save the sink name as a property.

    getInputsProcess->start(program, arguments); //start the process

    connect(getInputsProcess,SIGNAL(finished(int)),this,SLOT(directStream())); //directStream will run when the process has finished
}

void pulsecommander::changeSink(int sinkId)
{
    QString program = "/bin/sh";
    QStringList arguments;

    arguments << "-c" << "pactl list sink-inputs | grep 'Sink Input' | cut -d\"#\" -f2";
    QProcess *getInputsProcess = new QProcess(); //Process that runs the command.
    getInputsProcess->setProperty("sinkid", QVariant(sinkId)); //Save the sinkid as a property.

    getInputsProcess->start(program, arguments); //start the process

    connect(getInputsProcess,SIGNAL(finished(int)),this,SLOT(directStream())); //directStream will run when the process has finished
}

//Prints the standard output of the process
void pulsecommander::readStdOut()
{
    QProcess *proc = (QProcess*)QObject::sender();
    QString out =(QString)proc->readAllStandardOutput();
    if(out.isEmpty()){
        qDebug()<<"No sink inputs found";
    }
    else{
        qDebug()<< out;
    }
}


void pulsecommander::directStream() //Called when the process has finished
{
    QProcess *getInputsProcess = (QProcess*)QObject::sender(); //The process that has emitted SIGNAL(finished(int))
    QString sinkId = getInputsProcess->property("sinkid").toString(); //Retrieve the sink ID
    QString result = (QString)getInputsProcess->readAllStandardOutput(); //Get the input id as a string
    QList<QString> listOfInput = result.split('\n', QString::SkipEmptyParts); //Split up input id
    if(listOfInput.isEmpty()){
        qDebug() << "No inputs found";
        return;
    }
    foreach (QString inputId, listOfInput) {  //Loop through all the input ID
        QProcess *changeSinkProcess = new QProcess();
        QString program = "/usr/bin/pactl";
        QStringList arguments;
        arguments << "move-sink-input" << inputId << sinkId;

        changeSinkProcess->start(program, arguments);
        if(!changeSinkProcess->waitForFinished()){ //The command unable to finnish somehowe
            qDebug()<<"could not change the sink for the output";
        }
        else{
            qDebug()<<"Source #"<< inputId <<" changed to sink" << sinkId;
        }
    }
}


