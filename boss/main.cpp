#include "mainwindow.h"
#include "network.h"
#include "pulsecommander.h"
#include <QApplication>
#include <QDebug>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QList>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    QNetworkInterface iface = QNetworkInterface::interfaceFromName("wlan0");
    QList<QNetworkAddressEntry> entries = iface.addressEntries();
    //qDebug()<< entries.first().broadcast();
    foreach (QNetworkAddressEntry n, entries) {
        qDebug()<<n.broadcast();
        n.ip();
    }

    network *net = new network(0,33000);


    return a.exec();
}
