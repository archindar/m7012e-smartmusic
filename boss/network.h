#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QUdpSocket>
#include <list>
#include <qjson/parser.h>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QHostAddress>

struct channel {
    QString ip;
    quint16 aourport;       //our port for audio
    quint16 athierport;     //thier port for audio
    quint16 vourport;       //our port for video
    quint16 vthierport;     //thier port for video
    QTcpSocket *listener;
    QTcpSocket *speaker;
    bool msgsent;
    bool accepted;
    channel() : msgsent(false), accepted(false) {}
};

class network : public QObject
{
    Q_OBJECT
public:
    explicit network(QObject *parent = 0, quint16 port=20000);
    void startConnection(QHostAddress recadr, quint16 recport, quint16 ourport);
    ~network();
    void acceptConnection(bool accept);

signals:
    void exchangeDone(channel *comData);

public slots:
    void handleIncomming(); //handle establishment of communication with a new server.
    void readPortMsg();         //handle input to a socket.
    void conEstablished();
    void remoteDisconnected();

private slots:

private:
    void sendPortMsg();
    QTcpServer *tcpserver; //Listens for incomming traffic
    QUdpSocket *regListener; //Listens for a registration of a Employee sent out on the broadcast address
    channel *ch1;
    //Startpoints for the recieving port
    QString localSink;
    QHostAddress localip;

    QList<QTcpSocket*> listOfSockets;
};

#endif // NETWORK_H
