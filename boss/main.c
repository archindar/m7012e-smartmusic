#include "sm_boss_pa.h"

#include <stdio.h>

int main() {
    int ret;
    
    ret = sm_boss_pa_load();
    if (ret)
        return -1;
    
    ret = sm_boss_pa_getsinkindex("130.240.5.132");
    if (ret < 0)
        return -2;
    
    printf("found index: %i\n", ret);
    
    return 0;
}

