#-------------------------------------------------
#
# Project created by QtCreator 2013-12-10T08:50:48
#
#-------------------------------------------------

QT      += core gui
QT      += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BOSS
TEMPLATE = app

#LIBS += -L/usr/local/lib -lqjson
#LIBS += -lsm_boss_pa
LIBS += -L\libsm_boss_pa.so

SOURCES += main.cpp\
        mainwindow.cpp \
    network.cpp \
    tcplistener.cpp \
    pulsecommander.cpp \
    sm_boss_pa.c

HEADERS  += mainwindow.h \
    network.h \
    tcplistener.h \
    pulsecommander.h\
    sm_boss_pa.h

FORMS    += mainwindow.ui
