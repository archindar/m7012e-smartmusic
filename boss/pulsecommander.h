#ifndef PULSECOMMANDER_H
#define PULSECOMMANDER_H

#include <QObject>
#include <QHostAddress>
#include <QProcess>
#include <QDebug>

class pulsecommander : public QObject
{
    Q_OBJECT
public:
    explicit pulsecommander(QObject *parent = 0);
    void changeSink(QString sinkName);
    void changeSink(int sinkId);
signals:

public slots:


    void readStdOut();
    void directStream();
private:

};

#endif // PULSECOMMANDER_H
